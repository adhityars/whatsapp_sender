

<?php
include "koneksi.php";
/**
 * PHPExcel - Excel data import to MySQL database script example
 * ==============================================================================
 * 
 * @version v1.0: PHPExcel_excel_to_mysql_demo.php 2016/03/03
 * @copyright Copyright (c) 2016, http://www.ilovephp.net
 * @author Sagar Deshmukh <sagarsdeshmukh91@gmail.com>
 * @SourceOfPHPExcel https://github.com/PHPOffice/PHPExcel, https://sourceforge.net/projects/phpexcelreader/
 * ==============================================================================
 *
 */


set_time_limit(0);
 
require 'Classes/PHPExcel/IOFactory.php';

$inputfilename = basename($_FILES['filexlsx']['name']) ;
move_uploaded_file($_FILES['filexlsx']['tmp_name'], $inputfilename);
 
// beri permisi agar file xls dapat di baca
chmod($_FILES['filexlsx']['name'],0777);
$exceldata = array();

// Create connection
//$connect = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
//if (!$connect) {
    //die("Connection failed: " . mysqli_connect_error());
//}

//  Read your Excel workbook

try
{
    $inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
    $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
    $objPHPExcel = $objReader->load($inputfilename);
}
catch(Exception $e)
{
    die('Error loading file "'.pathinfo($inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn


		
for ($row = 2; $row <= $highestRow; $row++) //baris ke 2 (tanpa judul kolom)
{ 
	// Calculate the percentation http://stackoverflow.com/questions/15298071/progress-bar-with-mysql-query-with-php
	$percent = intval($row/$highestRow * 100)."%";

	// Javascript for updating the progress bar and information
  echo '<script language="javascript">
  document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
  document.getElementById("information").innerHTML="'.$row.' data No HP sedang diproses.";
  </script>';

  // This is for the buffer achieve the minimum size in order to flush data
  //echo str_repeat(' ',1024*64);

  // Send output to browser immediately
  //flush();

  // Sleep one second so we can see the delay
  //sleep(0);
  
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	
    //  Insert row data array into your database of choice here

	$sql = "INSERT INTO wa (no_hp, pesan, status_kirim)
	
	VALUES 
			(
			
			'".mysqli_real_escape_string($connect, $rowData[0][0])."',
			'".mysqli_real_escape_string($connect, $rowData[0][1])."',
			'0'		
			)";
			

			
	if (mysqli_query($connect, $sql)) {

		$exceldata[] = $rowData[0];

	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($connect);
	}
}


// Print excel data
 echo "<table>";
foreach ($exceldata as $index => $excelraw)
{
	echo "<tr>";
	foreach ($excelraw as $excelcolumn)
	{
		echo "<td>".$excelcolumn."</td>";
	}
	echo "</tr>";
}
echo "</table>"; 
//hapus kembali file .xls yang di upload tadi
unlink($_FILES['filexlsx']['name']);
 
// alihkan halaman ke index.php
header("location:index.php?berhasil=$berhasil");
//mysqli_close($connect);

// Tell user that the process is completed
// alihkan halaman ke index.php

echo '<script language="javascript">document.getElementById("information").innerHTML="Proses selesai"</script>';

?>
