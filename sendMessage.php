<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
$phone = isset($_GET['phone']) ? $_GET['phone'] : '';
$message = isset($_GET['message']) ? $_GET['message'] : '';


$arr = array();

if (empty($phone)) {
	$stat = "Error";
	$ket = "Phone is Empty";
}elseif(empty($message)){
	$stat = "Error";
	$ket = "Message Is Empty";
}else{
	$message = preg_replace( "/(\n)/", "<ENTER>", $message );
	$message = preg_replace( "/(\r)/", "<ENTER>", $message );

	$phone = preg_replace( "/(\n)/", ",", $phone );
	$phone = preg_replace( "/(\r)/", "", $phone );

	$data = array("phone_no" => $phone, "key" => "68c49843e6fd3e9275884548e26e348492e6155da5925141", "message" => $message);
	$data_string = json_encode($data);
	$ch = curl_init('http://116.203.92.59/api/send_message');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($data_string))
	);
	$result = curl_exec($ch);

	$stat = "Success";
	$ket = "Message Send";
}

$arr['status'] = $stat;
$arr['info'] = $ket;


echo json_encode($arr);

 ?>
