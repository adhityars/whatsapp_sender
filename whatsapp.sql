/*
Navicat MySQL Data Transfer

Source Server         : MYSQL
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : whatsapp

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2020-01-02 14:33:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `upload`
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(100) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of upload
-- ----------------------------
INSERT INTO `upload` VALUES ('1', '12.jpg');
INSERT INTO `upload` VALUES ('2', '13.jpg');
INSERT INTO `upload` VALUES ('3', '14.jpg');
INSERT INTO `upload` VALUES ('4', 'dreamstime_m_123790683.jpg');
INSERT INTO `upload` VALUES ('5', 'KTP.jpg');

-- ----------------------------
-- Table structure for `wa`
-- ----------------------------
DROP TABLE IF EXISTS `wa`;
CREATE TABLE `wa` (
  `no_hp` varchar(15) NOT NULL,
  `pesan` text NOT NULL,
  `status_kirim` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of wa
-- ----------------------------
INSERT INTO `wa` VALUES ('081380077386', 'ini test blast1', '1');
INSERT INTO `wa` VALUES ('6287724028098', 'ini test blast2', '1');
INSERT INTO `wa` VALUES ('081380077386', 'ini test blast1', '0');
INSERT INTO `wa` VALUES ('08121080610', 'ini test blast2', '0');
