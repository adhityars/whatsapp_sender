<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- icon -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body class="bg-light">
    <h1 class="bg-dark text-center text-white"><i class="fab fa-whatsapp" style="color:lime"></i> WHATSAPP ACTION</h1>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text-white font-weight-bold" style="background-color: green">
              SEND MANUAL 
            </div>
            <div class="card-body">
              <form action="whatsapptext.php" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="number" class="form-control form-control-sm" name="no_phone" placeholder="08120xxxxxxx">
                  <small id="emailHelp" class="form-text text-muted"><i>*masukan nomor selular</i></small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Pesan</label>
                  <textarea class="form-control form-control-sm" rows="7" name="content"></textarea>
                </div>
                <button type="submit" class="btn btn-primary float-right"><i class="fas fa-paper-plane"></i> Send</button>
              </form>
            </div>
          </div>
        </div>
<!-- ============================================================================================ -->
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text-white font-weight-bold" style="background-color: green">
              SEND AUTOMATIC UPLOAD
	    <span class="float-right"><a href="wa.xlsx" class="btn btn-sm btn-success">Download Format</a></span>
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data" action="insert_progress.php">
                <input name="filexlsx" type="file" required="required"> 
                <input name="upload" type="submit" value="Import">
              </form>
              <hr>
              <form method="post" enctype="multipart/form-data" action="wa_blast.php">
                <div class="form-group">
                  <div style="padding:0px;overflow:auto;width:100%;height:237px;border:0px solid grey" >
                    <table class="table table-sm table-striped" >
                      <tr>
                        <th>No</th>
                        <th>No HP</th>
                        <th>Pesan</th>
                      </tr>
                      <?php 
                      include 'koneksi.php';
                      $no=1;
                      $data = mysqli_query($connect,"select * from wa where status_kirim ='0' ");
                      while($d = mysqli_fetch_array($data)){
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $d['no_hp']; ?></td>
                          <td><?php echo $d['pesan']; ?></td>
                        </tr>
                        <?php 
                      }
                      ?>
                   
                    </table>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary float-right"><i class="fas fa-paper-plane"></i> Send</button>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
<br>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text-white font-weight-bold" style="background-color: green">
              WHATSAPP GAMBAR
            </div>
	    <div class="card-body">
	    <form method="post" enctype="multipart/form-data" action="wa_upload.php">
                <input name="file" type="file" required="required">
                <input name="upload" type="submit" value="Upload">
            </form>
		<br>
		<div style="padding:0px;overflow:auto;width:100%;height:237px;border:0px solid grey" >
	    <table class="table table-sm table-striped" >
                      <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama File</th>
		
                      </tr>
                      <?php
                      include 'koneksi.php';
                      $no=1;
                      $data = mysqli_query($connect,"select * from upload");
                      while($d = mysqli_fetch_array($data)){
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><img width="50" height="50"; src="<?php echo "".$d['nama_file']; ?>"></td>
                          <td><?php echo $d['nama_file'];?></td>
			</tr>
                        <?php
                      }
                      ?>

            </table>
	    </div>
	   </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text-white font-weight-bold" style="background-color: green">
              SEND MANUAL GAMBAR
            </div>
            <div class="card-body">
              <form action="wa_gbr.php" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="number" class="form-control form-control-sm" name="no_phone" placeholder="08120xxxxxxx">
                  <small class="form-text text-muted"><i>*masukan nomor selular</i></small>
                </div>
                <div class="form-group">
              		<label for="exampleInputEmail1">Nama File</label>
		     	<input type="text" class="form-control form-control-sm" name="attach">
			<small class="form-text text-muted"><i>*contoh : 123.jpg</i></small>
                </div>
                <button type="submit" class="btn btn-primary float-right"><i class="fas fa-paper-plane"></i> Send</button>
              </form>
            </div>
          </div>
        </div>
<!-- ============================================================================================ -->
        
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
